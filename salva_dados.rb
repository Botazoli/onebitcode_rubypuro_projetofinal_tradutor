require_relative 'traduzir_dados'
require_relative 'recebe_dados'

class SalvaDados < TraduzirDados

    def nomeia_arquivo
        time = Time.now
        time = time.strftime('%d-%m-%y_%H:%M')
        @nome_arquivo = time + ".txt" 
        puts "--" * 20
        puts "NOME DO ARQUIVO: #{@nome_arquivo}"
        puts "--" * 20
    end
   
    def salva_dados_arquivo
        file = File.open("#{@nome_arquivo}", 'a') do |line|
            line.puts("-----------------------")
            line.puts("TRADUZA: #{@@texto} DE: #{@@idioma_de} PARA: #{@@idioma_para}")
            line.puts("TRADUÇÃO: #{@@resultado}")
      end
    end   

end