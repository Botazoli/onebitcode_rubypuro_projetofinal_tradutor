require 'rest-client'
require 'json'
require_relative 'recebe_dados'

class TraduzirDados < RecebeDados
    def pegar_origem

        response = RestClient.post('https://translate.yandex.net/api/v1.5/tr.json/detect', 
        {
        key: 'trnsl.1.1.20200114T220437Z.09dbab97ba2df631.2c7e58e3512ff81f7895ae736ceab8d070c9a4a9',
        text: "#{@@idioma_de}"
        })
        @origem = JSON.parse(response)['lang']
    end

    def pegar_destino

        response = RestClient.post('https://translate.yandex.net/api/v1.5/tr.json/detect', 
        {
        key: 'trnsl.1.1.20200114T220437Z.09dbab97ba2df631.2c7e58e3512ff81f7895ae736ceab8d070c9a4a9',
        text: "#{@@idioma_para}"
        })
       puts @destino = JSON.parse(response)['lang']
    end


    def traduzido

        response = RestClient.post('https://translate.yandex.net/api/v1.5/tr.json/translate', 
        {
        key: 'trnsl.1.1.20200114T220437Z.09dbab97ba2df631.2c7e58e3512ff81f7895ae736ceab8d070c9a4a9',
        text: "#{@@texto}",
        lang: "#{@origem}-#{@destino}"
        })
        puts response.body
        puts @@resultado =JSON.parse(response.body)['text']
        puts "--" * 20
        puts "TRADUÇÃO: #{@@resultado}"
        puts "--" * 20
    end

end