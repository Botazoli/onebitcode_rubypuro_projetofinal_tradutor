class RecebeDados

    attr_accessor :texto, :idioma_de, :idioma_para      

    def pega_dados_formulario
        puts 'Digite o texto: '
        @@texto = gets.chomp
        puts 'Traduzir de: '
        @@idioma_de = gets.chomp
        puts 'Traduzir para: '
        @@idioma_para  = gets.chomp

        puts "--" * 20
        puts "TRADUZA: #{@@texto} DE: #{@@idioma_de} PARA: #{@@idioma_para}"
        puts "--" * 20
    end

end